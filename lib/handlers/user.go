package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"golang.org/x/crypto/bcrypt"
	"storeAPI/gen"
	"storeAPI/lib"
)

func LoginPost(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var t gen.UserLogin
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	user := &gen.User{
		Email: t.Email,
	}

	user, err = lib.Bolt.GetUser(user)

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(t.Password)); err != nil {
		log.Println("no such user")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var sid *string
	if sid, err = lib.Bolt.CreateSession(&t); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("X-Session-Id", *sid)
	w.WriteHeader(http.StatusOK)
}

func NewUserPost(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var t gen.User
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(t.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Printf("generate password hash error: %v", body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Password = string(hash)

	if err = lib.Bolt.CreateUser(&t); err != nil {
		log.Printf("vreate user error: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}
