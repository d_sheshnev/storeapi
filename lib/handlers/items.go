package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"storeAPI/gen"
	"storeAPI/lib"
	"storeAPI/lib/models"
)

func AddPost(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var t gen.ItemCreate
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	item := gen.Item{
		Name:     t.Name,
		Quantity: t.Quantity,
		Price:    t.Price,
	}

	var newItem *gen.Item
	if newItem, err = lib.Bolt.CreateItem(&item); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	payload, err := json.Marshal(newItem)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(payload)
}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var t gen.Body
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = lib.Bolt.DeleteItem(&gen.Item{
		ItemId: t.Id,
	})
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func ListGet(w http.ResponseWriter, r *http.Request) {

	var items models.Items
	var err error
	if items.Items, err = lib.Bolt.GetItems(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	payload, err := json.Marshal(items)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(payload)
}

func UpdatePost(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var t gen.Item
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("incorrect data: %v", body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err = lib.Bolt.UpdateItem(&t); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(body)
}
