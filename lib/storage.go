package lib

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"

	"storeAPI/gen"

	"github.com/coreos/bbolt"
	"github.com/satori/go.uuid"
	"strconv"
)

var Bolt BoltDB

const (
	RootBucket     string = "ROOT"
	UsersBucket    string = "USERS"
	SessionsBucket string = "SESSIONS"
	ItemsBucket    string = "ITEMS"
)

type BoltDB struct {
	db *bolt.DB
}

func init() {
	db, err := bolt.Open("storeAPI.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal(err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		root, err := tx.CreateBucketIfNotExists([]byte(RootBucket))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		for _, value := range []string{UsersBucket, ItemsBucket, SessionsBucket} {
			_, err = root.CreateBucketIfNotExists([]byte(value))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
		}

		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
	Bolt.db = db
}

func (b *BoltDB) CreateUser(user *gen.User) error {
	u := user

	err := b.db.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(UsersBucket))

		if buf, err := json.Marshal(u); err != nil {
			return err
		} else if err := usersBkt.Put([]byte(u.Email), buf); err != nil {
			return err
		}

		return nil
	})

	return err
}

func (b *BoltDB) GetUser(user *gen.User) (*gen.User, error) {
	var userJson []byte

	err := b.db.View(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(UsersBucket))

		userJson = usersBkt.Get([]byte(user.Email))
		return nil
	})

	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(userJson, &user); err != nil {
		return nil, err
	}

	return user, nil
}

func (b *BoltDB) CreateItem(item *gen.Item) (*gen.Item, error) {
	newItem := item
	err := b.db.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		bkt := root.Bucket([]byte(ItemsBucket))

		itemID, err := bkt.NextSequence()
		if err != nil {
			return err
		}

		newItem.ItemId = string(itemID)

		if buf, err := json.Marshal(newItem); err != nil {
			return err
		} else if err := bkt.Put([]byte(newItem.ItemId), buf); err != nil {
			return err
		}
		return nil
	})

	return newItem, err
}

func (b *BoltDB) UpdateItem(item *gen.Item) error {

	err := b.db.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		itemsBkt := root.Bucket([]byte(ItemsBucket))

		oldItem := itemsBkt.Get([]byte(item.ItemId))
		if len(oldItem) == 0 {
			return errors.New("update item: no such item")
		}

		if buf, err := json.Marshal(item); err != nil {
			return err
		} else if err := itemsBkt.Put([]byte(item.ItemId), buf); err != nil {
			return err
		}

		return nil
	})

	return err
}

func (b *BoltDB) DeleteItem(item *gen.Item) error {
	return b.db.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(ItemsBucket))

		err := usersBkt.Delete([]byte(item.ItemId))
		return err
	})
}

func (b *BoltDB) GetItem(item *gen.Item) (*gen.Item, error) {
	var itemJson []byte
	err := b.db.View(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(ItemsBucket))

		itemJson = usersBkt.Get([]byte(item.ItemId))
		return nil
	})

	if err != nil {
		return nil, err
	}

	var dbItem *gen.Item
	if err := json.Unmarshal(itemJson, dbItem); err != nil {
		return nil, err
	}

	return dbItem, nil
}

func (b *BoltDB) GetItems() ([]gen.Item, error) {
	var itemsJson [][]byte
	err := b.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		root := tx.Bucket([]byte(RootBucket))
		itemsBkt := root.Bucket([]byte(ItemsBucket))

		c := itemsBkt.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			itemsJson = append(itemsJson, v)
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	var items []gen.Item
	for _, v := range itemsJson {
		var i gen.Item
		err := json.Unmarshal(v, &i)
		if err == nil {
			items = append(items, i)
		} else {
			log.Printf("db get items: err unmarashalling item %v", v)
		}
	}

	return items, nil
}

func (b *BoltDB) CreateSession(user *gen.UserLogin) (*string, error) {
	sid := uuid.Must(uuid.NewV4()).String() + "|" + strconv.FormatInt(time.Now().Unix(), 10)

	err := b.db.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(UsersBucket))

		if err := usersBkt.Put([]byte(sid), []byte(user.Email)); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return nil, err
	}
	return &sid, nil
}

func (b *BoltDB) DeleteSession(sid string) error {
	return b.db.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(UsersBucket))

		return usersBkt.Delete([]byte(sid))
	})
}

func (b *BoltDB) GetSession(sid string) (*string, error) {
	var user []byte
	err := b.db.View(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(RootBucket))
		usersBkt := root.Bucket([]byte(UsersBucket))
		user = usersBkt.Get([]byte(sid))
		return nil
	})

	if err != nil {
		return nil, err
	}
	mail := string(user)
	return &mail, nil
}
