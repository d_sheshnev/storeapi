package middleware

import (
	"net/http"
	"storeAPI/lib"
	"context"
)

func AuthRequired(next func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		sid := r.Header["X-Session-Id"]

		if len(sid) < 1 {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		// todo: Check user status for permissions (if user.Status > 1 for example)
		email, err := lib.Bolt.GetSession(sid[0])
		if err != nil || email == nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		ctx := context.WithValue(r.Context(), "Email", email)
		next(w,r.WithContext(ctx))
	}
}
