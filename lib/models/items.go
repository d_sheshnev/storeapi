package models

import "storeAPI/gen"

type Items struct {
	Items []gen.Item `json:"items"`
}